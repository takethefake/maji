const NodeGit = require("nodegit");
const fs = require("fs");
const sha = require("sha256");

GitRepo = {
  openRepo: repoURL =>
    GitRepo.repoAlreadyExist(repoURL)
      ? GitRepo.open(repoURL)
      : GitRepo.clone(repoURL),
  repoAlreadyExist: repoURL => fs.existsSync(GitRepo.localRepoPath(repoURL)),
  localRepoPath: repoURL => "/tmp/" + GitRepo.hashRepoURL(repoURL),
  hashRepoURL: repoURL => sha(repoURL),
  blame: (repo, file) =>
    new Promise((resolve, reject) => {
      NodeGit.Blame.file(repo, file, "-p").then(blame => {
        let count = 1;
        fileContributors = [];
        for (let i = 0; i < blame.getHunkCount(); i++) {
          let hunk = blame.getHunkByIndex(i);
          for (let j = 0; j < hunk.linesInHunk(); j++) {
            const usermail = hunk
              .finalSignature()
              .email()
              .toString();
            fileContributors[usermail] = fileContributors[usermail]
              ? {
                  email: usermail,
                  count: fileContributors[usermail].count + 1
                }
              : {
                  email: usermail,
                  count: 1
                };
            console.log(
              count +
                ":" +
                hunk
                  .finalSignature()
                  .email()
                  .toString()
            );
            count++;
          }
        }
        fileContributors = Object.values(fileContributors).sort(function(b, a) {
          return a.count - b.count;
        });
        resolve(fileContributors);
      });
    }),
  clone: repoURL =>
    new Promise((resolve, reject) => {
      console.log("clones Repo");
      const cloneOptions = {};
      const cloneRepository = NodeGit.Clone(
        repoURL,
        GitRepo.localRepoPath(repoURL),
        cloneOptions
      );
      cloneRepository
        .then(repository => {
          resolve(repository);
        })
        .catch(e => {
          reject(e);
        });
    }),
  open: repoURL => {
    return new Promise((resolve, reject) => {
      console.log("opens Repo");
      NodeGit.Repository.open(GitRepo.localRepoPath(repoURL))
        .then(function(repo) {
          resolve(repo);
        })
        .catch(e => reject(e));
    });
  }
};

module.exports = GitRepo;
