const axios = require("axios");
const API_BASE_URL = "https://git.schulz.codes/api/v4";
const secretToken = "wm3KeeKJ5Z1ensYu342d";
const getApiURL = (url, queryString) => {
  return `${API_BASE_URL}${url}?private_token=${secretToken}${
    queryString ? `&${queryString}` : ""
  }`;
};

const gitActions = {
  getUserById: userId => {
    return axios
      .get(getApiURL(`/users/${userId}`))
      .then(result => {
        const { data } = result;
        return data;
      })
      .catch(err => {
        console.log(err);
        return err;
      });
  },
  getUserIdByName: name => {
    return axios
      .get(getApiURL("/users", `search=${encodeURIComponent(name)}`))
      .then(result => {
        const { data } = result;
        if (data.length) {
          const { id } = data[0];
          return id;
        }
        throw new Error("No user found.");
      })
      .catch(err => {
        console.log(err);
        return err;
      });
  },
  getUserIdByData: ({ email, name }) => {
    return axios
      .get(getApiURL("/users", `search=${encodeURIComponent(email)}`))
      .then(result => {
        const { data } = result;
        if (data.length) {
          const { id } = data[0];
          return id;
        } else {
          return gitActions.getUserIdByName(name);
        }
      })
      .catch(err => {
        console.log(err);
        return err;
      });
  },
  isMergeRequestUnassigned: (projectId, mergeRequestId) => {
    return axios
      .get(getApiURL(`/projects/${projectId}/merge_requests/${mergeRequestId}`))
      .then(result => {
        const { data: { assignee } } = result;
        return !assignee;
      })
      .catch(err => {
        console.log(err);
      });
  },
  getMergeRequestAuthor: (projectId, mergeRequestId) => {
    return axios
      .get(getApiURL(`/projects/${projectId}/merge_requests/${mergeRequestId}`))
      .then(result => {
        const { data: { author: { id } } } = result;
        return gitActions.getUserById(id).then(data => {
          const { email, username } = data;
          return {
            email,
            username
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  },
  updateMergeRequest: (projectId, mergeRequestId, userId) => {
    /*return axios
      .put(
        getApiURL(`/projects/${projectId}/merge_requests/${mergeRequestId}`),
        { assignee_id: userId }
      )
      .then(result => {
        console.log(result);
      });*/
  },
  getContributors: (projectId, targetBranch) => {
    return axios
      .get(
        getApiURL(
          `/projects/${projectId}/repository/commits`,
          `ref_name=${targetBranch}`
        )
      )
      .then(result => {
        const { data } = result;
        const users = data.reduce((userArr, val) => {
          const { committer_email, committer_name } = val;
          if (!userArr.find(usr => usr.email === committer_email)) {
            userArr.push({
              email: committer_email,
              name: committer_name
            });
          }
          return userArr;
        }, []);
        return users;
      });
  },
  getMergeRequestChangesById: (projectId, mergeRequestId) => {
    return axios
      .get(
        getApiURL(
          `/projects/${projectId}/merge_requests/${mergeRequestId}/changes`
        )
      )
      .then(response => {
        const { data: { changes } } = response;
        return changes;
      });
  }
};

const Gitlab = {
  getProjects: () => axios.get("http://git.schulz.codes/api/v4/projects"),
  getMergeRequests: id =>
    axios.get(
      `http://git.schulz.codes/api/v4/projects/${id}/merge_requests?state=opened`
    )
};

module.exports = { gitActions, Gitlab };
