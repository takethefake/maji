const GitRepo = require("./repo");
const { gitActions } = require("./gitlab");

const userPicker = {
  pickReviewer: (projectId, mergeRequestId, repoURL) => {
    console.log(projectId, mergeRequestId, repoURL);
    return GitRepo.openRepo(repoURL).then(repo => {
      gitActions
        .getMergeRequestChangesById(projectId, mergeRequestId)
        .then(changes => {
          changes.map(obj => {
            console.log(obj.old_path);
            GitRepo.blame(repo, obj.old_path)
              .then(blame => {
                console.log(blame);
                return blame;
              })
              .catch(e => {
                console.log(e);
              });
          });
        })
        .catch(e => {
          console.log(e);
        });
    });
  }
};

module.exports = userPicker;
