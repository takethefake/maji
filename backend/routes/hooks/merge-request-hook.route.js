const express = require("express");
const router = express.Router();
const userPicker = require("../../gitlab/userPicker");
const { gitActions } = require("../../gitlab/gitlab");

const routes = {
  /* GET users listing. */
  mergeRequest: router.post("/merge-request", function(req, res, next) {
    const { body: data } = req;
    const {
      project: { id: projectID, http_url: repoURL },
      object_attributes: { iid: mergeRequestID, target_branch: targetBranch }
    } = data;
    gitActions
      .isMergeRequestUnassigned(projectID, mergeRequestID)
      .then(isUnassigned => {
        if (isUnassigned) {
          gitActions
            .getMergeRequestAuthor(projectID, mergeRequestID)
            .then(author => {
              console.log(author);
              gitActions
                .getContributors(projectID, targetBranch)
                .then(users => {
                  const filteredByAuthor = users.filter(
                    user => user.email !== author.email
                  );
                  userPicker.pickReviewer(projectID, mergeRequestID, repoURL);
                  /*const chosen =
                    filteredByAuthor[
                      Math.floor(Math.random() * filteredByAuthor.length)
                    ];
                  gitActions.getUserIdByData(chosen).then(userId => {
                    /*gitActions.updateMergeRequest(
                      projectID,
                      mergeRequestID,
                      userId
                    );
                  });*/
                })
                .catch(err => {
                  console.log(err);
                });
            });
        }
      });
  })
};

module.exports = routes;
