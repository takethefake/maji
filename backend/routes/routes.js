const routes = {
  GIT_RECEIVE_CODE: "/api/git/auth/receive",
  ASSIGN_MERGE_REQUEST: "/api/git/merge-request/:id/assign",
  GET_PROJECTS: "/api/projects",
  GET_PROJECT: "/api/projects/:id"
};
module.exports = routes;
