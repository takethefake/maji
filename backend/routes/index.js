const express = require("express");
const router = express.Router();

const { gitActions } = require("../gitlab/gitlab");

const gitAuth = require("./git/auth.git.route");
const mergeRequestHook = require("./hooks/merge-request-hook.route");
const projects = require("./projects/projects.route");

router.use("/hooks", mergeRequestHook.mergeRequest);
router.use("/git", gitAuth.receive);
router.use("/projects", projects.project);
router.use("/projects", projects.projects);

module.exports = router;
