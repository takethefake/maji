const express = require("express");
const router = express.Router();
const git = require("../../gitlab/gitlab");

const routes = {
  /* GET project listing. */
  projects: router.get("/", async function(req, res, next) {
    const projects = (await git.Gitlab.getProjects()).data;

    res.send(
      projects.map(project => ({
        id: project.id,
        name: project.name_with_namespace,
        lastActivity: +new Date(project.last_activity_at),
        url: project.web_url,
        avatar: project.avatar || "https://i.imgur.com/tH0FlyK.png",
        createdAt: +new Date(project.created_at),
        latestReview: 1516383856625,
        lastReviewer: "Daniel Schulz"
      }))
    );
  }),

  project: router.get("/:id/mergeRequests", async function(req, res, next) {
    const { params: { id } } = req;

    const mergeRequests = (await git.Gitlab.getMergeRequests(id)).data;

    console.log(mergeRequests[0]);
    res.send(
      mergeRequests.map(mr => ({
        id: mr.id,
        wip: mr.work_in_progress,
        state: mr.state,
        author: mr.author,
        title: mr.title,
        milestone: mr.milestone,
        assignee: mr.assignee
      }))
    );
  })
};

module.exports = routes;
