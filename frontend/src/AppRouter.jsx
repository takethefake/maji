import React from 'react';
import { connect } from 'react-redux';
import { routerActions } from 'react-router-redux';
import { Route, Router, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { history } from './store/configureStore';
import * as urls from './config/urls.config';

import Home from './screens/Home/Home.screen';
import Dashboard from './screens/Dashboard/Dashboard.screen';

import Header from './components/Header/Header.component';

const AppRouter = () => (
  <Router history={history}>
    <div>
      <Header />
      <Switch>
        <Route exact path={urls.client.home} component={Home} />
        <Route path={urls.client.dashboard} component={Dashboard} />
      </Switch>
    </div>
  </Router>
);

const mapDispatchToProps = dispatch => ({
  routing: bindActionCreators(routerActions, dispatch),
});

export default connect(null, mapDispatchToProps)(AppRouter);
