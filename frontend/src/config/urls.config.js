export const client = {
  home: '/',
  dashboard: '/projects/:projectId',
};

export const api = {
  projects: '/api/projects',
  projectMergeRequests: id => `/api/projects/${id}/mergeRequests`,
};
