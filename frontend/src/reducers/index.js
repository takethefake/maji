import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import project from './project.reducer';

export default combineReducers({
  routing: routerReducer,
  project,
});
