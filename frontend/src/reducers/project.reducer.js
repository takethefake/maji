import * as actionTypes from '../actions/project.actions';

const initialState = {
  loading: true,
  items: [],
  selected: null,
};

export default function fetchProject(state = initialState, action) {
  switch (action.type) {
    case actionTypes.PROJECT_FETCH_MULTIPLE_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.PROJECT_FETCH_MULTIPLE_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload,
      };
    case actionTypes.PROJECT_FETCH_MULTIPLE_ERROR:
      return {
        ...state,
        loading: false,
      };

    case actionTypes.PROJECT_FETCH_SINGLE_SUCCESS:
      return {
        ...state,
        selected: action.payload,
      };

    default:
      return state;
  }
}
