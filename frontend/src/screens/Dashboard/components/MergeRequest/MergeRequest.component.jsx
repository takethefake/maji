// @flow
import React from 'react';
import Link from 'react-router-dom/Link';
import { css } from 'glamor';
import theme from '../../../../theme';
import Tile from '../../../../components/Tile/Tile.component';

type Props = {
  coderImage: String,
  reviewerImage: String,
  coderName: String,
  reviewerName: String,
  title: String,
  id: String,
  projectId: String,
};

const styles = {
  container: css({
    height: '200px',
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    padding: '55px 10px 10px',
    backgroundColor: theme.primary,
  }),

  img: css({
    position: 'absolute',
    width: '100px',
    height: '100px',
    top: '-60px',
    borderRadius: '50%',
  }),

  nameContainer: css({
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    opacity: 0.3,
  }),

  left: css({
    left: '10px',
  }),

  right: css({
    right: '10px',
  }),

  title: css({
    fontSize: '2rem',
    marginTop: '75px',
    textAlign: 'center',
  }),

  ticket: css({
    fontSize: '1.5rem',
    opacity: 0.6,
  }),

  resetLink: css({
    color: 'black',
    textDecoration: 'none',
  }),
};

export default (props: Props) => (
  <Tile style={{ width: '400px', marginBottom: '85px' }}>
    <Link
      {...styles.container}
      {...styles.resetLink}
      to={`/projects/${props.projectId}/mergeRequests/${props.id}`}
      href="/"
    >
      <img
        {...styles.img}
        {...styles.left}
        src={props.coderImage}
        alt={props.coderName}
        title={props.coderName}
      />
      <img
        {...styles.img}
        {...styles.right}
        src={props.reviewerImage}
        alt={props.reviewerName}
        title={props.reviewerName}
      />
      <div {...styles.nameContainer}>
        <div>
          <span>{props.coderName}</span>
        </div>
        <div>
          <span>{props.reviewerName}</span>
        </div>
      </div>
      <span {...styles.title}>{props.title}</span>
    </Link>
  </Tile>
);
