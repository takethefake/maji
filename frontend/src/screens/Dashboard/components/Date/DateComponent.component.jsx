// @flow
import React from 'react';
import { css } from 'glamor';
import DatePicker from 'react-datepicker';

import theme from './../../../../theme';

const styles = {
  container: css({
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  }),

  timelineContainer: css({
    background: theme.contrast,
  }),
};

type Props = {
  startDate: Date,
  endDate: Date,
  handleChangeStart: Function,
  handleChangeEnd: Function,
};

const DateComponent = (props: Props) => (
  <div {...styles.container}>
    <div {...styles.container} {...styles.timelineContainer}>
      <div {...styles.dot}>
        <DatePicker
          dateFormat="DD.MM.YYYY"
          todayButton="Today"
          selected={props.startDate}
          onChange={props.handleChangeStart}
        />
      </div>
      <div {...styles.dot}>
        <DatePicker
          dateFormat="DD.MM.YYYY"
          todayButton="Today"
          selected={props.endDate}
          selectsEnd
          startDate={props.startDate}
          endDate={props.endDate}
          onChange={props.handleChangeEnd}
        />
      </div>
    </div>
  </div>
);

export default DateComponent;
