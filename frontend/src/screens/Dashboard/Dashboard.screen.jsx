// @flow
import React from 'react';
import { connect } from 'react-redux';
import { css } from 'glamor';
import { bindActionCreators } from 'redux';
import 'react-datepicker/dist/react-datepicker.css';

import Loader from '../../components/Loader/Loader.component';
import * as projectActions from '../../actions/project.actions';
import MergeRequest from './components/MergeRequest/MergeRequest.component';

const styles = {
  container: css({
    padding: '50px',
  }),
  title: css({
    fontSize: '2rem',
  }),

  mergeRequests: css({
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  }),
};

type Props = {
  projectActions: Function[],
  match: Object,
  mergeRequests: Object[],
  projects: Object[],
};

class Dashboard extends React.Component<Props> {
  componentDidMount() {
    if (!this.props.projects.length) {
      this.props.projectActions.fetchProjects();
    }
    this.props.projectActions.fetchProject(this.props.match.params.projectId);
  }

  render() {
    if (!this.props.mergeRequests) {
      return <Loader />;
    }

    const matchingProject = this.props.projects.find(
      p => p.id === +this.props.match.params.projectId,
    );

    return (
      <div
        {...css({
          padding: '50px',
        })}
      >
        <div>
          <span {...styles.title}>
            {matchingProject && matchingProject.name}
          </span>
        </div>
        <div style={{ marginBottom: '60px' }}>
          <span>
            Number of tickets to be reviewed: {this.props.mergeRequests.length}
          </span>
        </div>
        <div {...styles.mergeRequests}>
          {this.props.mergeRequests.map(mr => (
            <MergeRequest
              key={mr.title}
              coderImage={mr.author.avatar_url}
              reviewerImage={mr.assignee.avatar_url}
              coderName={mr.author.name}
              reviewerName={mr.assignee.name}
              title={mr.title}
              id={mr.id}
              projectId={this.props.match.params.projectId}
            />
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  projects: state.project.items,
  mergeRequests: state.project.selected,
});

const mapDispatchToProps = dispatch => ({
  projectActions: bindActionCreators(projectActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
