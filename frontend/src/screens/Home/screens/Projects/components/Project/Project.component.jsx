// @flow
import React from 'react';
import { css } from 'glamor';
import moment from 'moment';
import { Link } from 'react-router-dom';

import Tile from '../../../../../../components/Tile/Tile.component';
import { flexCol } from '../../../../../../theme';

type Props = {
  name: String,
  lastActivity: Number,
  href: String,
  avatar: String,
  createdAt: Number,
  latestReview?: Date,
  lastReviewer: String,
  id: String,
};

const styles = {
  container: css({
    height: '200px',
    width: '100%',
    display: 'flex',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-between',
  }),

  image: css({
    borderRadius: '50%',
    height: '100%',
  }),

  name: css({
    fontSize: '4rem',
  }),

  informationContainer: css({
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
    alignItems: 'center',
  }),

  middle: css({
    justifyContent: 'center',
    align: 'center',
    textAlign: 'center',
  }),

  paleText: css({
    opacity: 0.4,
  }),

  resetLink: css({
    color: 'black',
    textDecoration: 'none',
  }),
};

const Project = (props: Props) => {
  const {
    name,
    lastActivity,
    avatar,
    createdAt,
    latestReview,
    lastReviewer,
    id,
  } = props;

  return (
    <Tile style={{ marginBottom: '20px' }}>
      <Link {...styles.resetLink} to={`/projects/${id}`} href="/">
        <div {...styles.container}>
          <div {...styles.informationContainer}>
            <img {...styles.image} src={avatar} alt={name} title={name} />
          </div>
          <div {...styles.informationContainer} {...styles.middle}>
            <span {...styles.name}>{name}</span>
            <span>Last Activity</span>
            <span {...styles.paleText}>{moment(lastActivity).calendar()}</span>
          </div>

          <div {...styles.informationContainer}>
            <div {...flexCol} style={{ width: '200px' }}>
              <span>Created At</span>
              <span {...styles.paleText}>{moment(createdAt).calendar()}</span>
            </div>
            {latestReview && (
              <div {...flexCol} style={{ width: '200px' }}>
                <span>Latest Review</span>
                <span {...styles.paleText}>
                  {moment(latestReview).calendar()}
                </span>
              </div>
            )}
            <div {...flexCol} style={{ width: '200px' }}>
              <span>Last Reviewer</span>
              <span {...styles.paleText}>{lastReviewer}</span>
            </div>
          </div>
        </div>
      </Link>
    </Tile>
  );
};

Project.defaultProps = {
  latestReview: null,
};

export default Project;
