// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { css } from 'glamor';
import { bindActionCreators } from 'redux';

import Loader from '../../../../components/Loader/Loader.component';
import Project from './components/Project/Project.component';
import * as projectActions from '../../../../actions/project.actions';

const styles = {
  container: css({
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  }),

  title: css({
    fontSize: '2rem',
    marginBottom: '10px',
  }),
};

type Props = {
  projectActions: Function,
  loading: Boolean,
  items: Array,
};

class Projects extends Component<Props> {
  componentWillMount() {
    this.props.projectActions.fetchProjects();
  }

  render() {
    if (this.props.loading) {
      return <Loader />;
    }
    return (
      <section {...styles.container}>
        <span {...styles.title}>Projects</span>
        {this.props.items.map(project => (
          <Project key={project.name} {...project} />
        ))}
      </section>
    );
  }
}

const mapStateToProps = state => ({ ...state.project });

const mapDispatchToProps = dispatch => ({
  projectActions: bindActionCreators(projectActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
