import React from 'react';
import { css } from 'glamor';

import Projects from './screens/Projects/Projects.screen';

export default () => (
  <div
    {...css({
      padding: '50px',
      display: 'flex',
      alignItems: 'center',
    })}
  >
    <Projects />
  </div>
);
