import React from 'react';
import { css } from 'glamor';

import Tile from '../Tile/Tile.component';

const rule = css({
  width: '100%',
  height: '50px',
});

export default () => (
  <header {...rule}>
    <Tile>Maji - Your Merge Request Supervisor</Tile>
  </header>
);
