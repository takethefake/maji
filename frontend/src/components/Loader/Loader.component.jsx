import React from 'react';
import LoadingGif from './images/Loader.image.gif';

export default () => (
  <img
    src={LoadingGif}
    alt="Loading..."
    style={{ height: '100%', margin: '0 auto' }}
  />
);
