// @flow
import React, { Node } from 'react';
import { css } from 'glamor';
import theme from '../../theme';

type Props = {
  children: Node,
  style: Object,
};

const rules = css({
  padding: '10px',
  background: theme.primary,
  width: '100%',
  boxShadow: '1px 1px 10px 1px rgba(0, 0, 0, 0.25)',
});

export default (props: Props) => (
  <div {...rules} style={props.style}>
    {props.children}
  </div>
);
