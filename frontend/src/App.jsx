import React from 'react';
import { css } from 'glamor';
import 'glamor/reset';

import AppRouter from './AppRouter';
import theme from './theme';

const rule = css({
  width: '100vw',
  height: '100%',
  background: theme.background,
  fontFamily: "'Raleway', sans-serif",
});

export default () => (
  <div {...rule}>
    <AppRouter />
  </div>
);
