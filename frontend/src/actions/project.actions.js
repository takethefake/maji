import axios from 'axios';
import * as urls from '../config/urls.config';

export const PROJECT_FETCH_MULTIPLE_START = 'PROJECT_FETCH_MULTIPLE_START';
export const PROJECT_FETCH_MULTIPLE_SUCCESS = 'PROJECT_FETCH_MULTIPLE_SUCCESS';
export const PROJECT_FETCH_MULTIPLE_ERROR = 'PROJECT_FETCH_MULTIPLE_ERROR';

export const PROJECT_FETCH_SINGLE_START = 'PROJECT_FETCH_SINGLE_START';
export const PROJECT_FETCH_SINGLE_SUCCESS = 'PROJECT_FETCH_SINGLE_SUCCESS';
export const PROJECT_FETCH_SINGLE_ERROR = 'PROJECT_FETCH_SINGLE_ERROR';

export function fetchProjects() {
  return dispatch => {
    dispatch({
      type: PROJECT_FETCH_MULTIPLE_START,
    });

    axios
      .get(urls.api.projects)
      .then(res => {
        dispatch({
          type: PROJECT_FETCH_MULTIPLE_SUCCESS,
          payload: res.data,
        });
      })
      .catch(() => {
        dispatch({
          type: PROJECT_FETCH_MULTIPLE_ERROR,
        });
      });
  };
}

export function fetchProject(id) {
  return dispatch => {
    dispatch({
      type: PROJECT_FETCH_SINGLE_START,
    });

    axios
      .get(urls.api.projectMergeRequests(id))
      .then(res => {
        dispatch({
          type: PROJECT_FETCH_SINGLE_SUCCESS,
          payload: res.data,
        });
      })
      .catch(() => {
        dispatch({
          type: PROJECT_FETCH_SINGLE_ERROR,
        });
      });
  };
}
