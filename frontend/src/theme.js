import { css } from 'glamor';

export default {
  accent: '#3498db', // LIGHT BLUE
  primary: 'white', // WHITE
  secondary: '#bdc3c7', // LIGHT-gray
  tertiary: '#95a5a6', // gray
  background: '#ecf0f1',
  contrast: '#333',
};

export const flex = css({
  display: 'flex',
});

export const flexCol = css({
  display: 'flex',
  flexDirection: 'column',
});
